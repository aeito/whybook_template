  <nav id="the-nav" class="navbar navbar-default col-md-5" role="navigation">

    <ul class="nav navbar-header col-md-3">
      <li class="col-md-1"><a href="?page=/ask">Posez une&nbsp;question</a></li>
      <li class="col-md-1"><a href="?page=/read">Consultez les&nbsp;réponses</a></li>
      <li class="col-md-1"><a href="?page=/who">Qui sommes-nous?</a></li>
    </ul>
  
    <ul class="nav navbar-nav navbar-right col-md-2">
      <li class="col-md-1">
        <div class=" input-group input-group-sm">
          <input type="text" class="form-control" placeholder="Search">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
          </span>
        </div>
      </li><li class="col-md-quater">
        <button type="button" class="btn btn-default btn-xs">Connexion</button>
      </li><li class="col-md-quater">
        <button type="button" class="btn btn-default btn-xs">Inscription</button>
      </li>
      <!-- <li class="dropdown">
        <a href="?page=home" role="button" data-toggle="dropdown" data-target="#"><span class="glyphicon glyphicon-search"></span> <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li>
            <form role="search">
              <div class="input-group input-group-sm">
                <input type="text" class="form-control">
                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit"></button>
                </div>
              </div>
            </form>
          </li>
        </ul>
      </li>
       -->
    </ul>

  </nav><!-- /the-nav -->