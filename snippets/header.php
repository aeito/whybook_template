<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Whybook | <?= $_GET['page'] ?></title>

  <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']) ?>/css/styles.css">
</head>
<body>
  
  <header id="the-header" class="col-md-2">

    <h1><img src="http://<?= $_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']) ?>/css/logo.svg" alt="Whybook"></h1>

  </header><!-- /the-header -->