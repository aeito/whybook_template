<aside id="the-message" class="alert-dismissable clearfix jumbotron"><!--  col-md-5 col-md-offset-1"> -->
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h2>Posez une question<br> et formez votre tr<span>oO</span>peau</h2>
  <p>whyb<span>oO</span>k envoie la question, vous notez la réponse.</p>
  <p><a href="?page=ask" class="btn btn-primary btn-lg" role="button">Posez une question</a></p>
</aside>