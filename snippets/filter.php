<nav id="the-filter">
  
  <h2>Sélection</h2>
  
  <ul class="filter selection">
    <li><a href="#/?filter=recent" class="active">Récent</a></li>
    <li><a href="#/?filter=popular">Populaire</a></li>
    <li><a href="#/?filter=staff-pick">Coup de cœur</a></li>
  </ul>
  
  <h2>Catégories</h2>
  
  <ul class="filter categories">
  <? foreach ($categories as $category): ?>
    <li><a href="#/?filter=<?= $category->safename() ?>"><?= $category->name() ?></a></li>
  <? endforeach ?>
  </ul>
  

</nav><!-- /the-filter -->