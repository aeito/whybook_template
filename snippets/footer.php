  <footer id="the-footer">
    <ul class="btn-group btn-group">
      <li class="btn btn-tertiary">Nous contacter</li>
      <li class="btn btn-tertiary">Plan du site</li>
      <li class="btn btn-tertiary">Mentions légales</li>
      <!-- <li class="btn btn-primary">primary</li>
      <li class="btn btn-secondary">secondary</li>
      <li class="btn btn-tertiary">tertiary</li>
      <li class="btn btn-success">success</li>
      <li class="btn btn-info">info</li>
      <li class="btn btn-warning">warning</li>
      <li class="btn btn-danger">danger</li> -->
    </ul>
    <small><p>© 2013-2014 Whyb<span>oO</span>k. Tous droits réservés.</p></small>
  </footer><!-- /the-footer -->
  
  <script src="js/jquery-2.1.0.min.js"         type="text/javascript" charset="utf-8"></script>
  <script src="css/bootstrap/js/transition.js" type="text/javascript" charset="utf-8"></script>
  <script src="css/bootstrap/js/dropdown.js"   type="text/javascript" charset="utf-8"></script>
  <script src="css/bootstrap/js/alert.js"      type="text/javascript" charset="utf-8"></script>
  <script src="js/script.js"                   type="text/javascript" charset="utf-8"></script>
</body>
</html>