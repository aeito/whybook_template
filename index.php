<?
  // embed a template snippet from the snippet folder
  function snippet($snippet) {
    global $questions,$question,$answer,$categories;
    include 'snippets/' . $snippet . '.php';
  }

  // object for dev.
  global $faker, $categories;
  require_once 'library/faker/src/autoload.php';

 class category
  {
    public $f, $q;

    public function __construct($faker)
    {
      $this->f = $faker;
      $this->q = $faker;
    }
    public function name()     { return $this->q; }
    public function safename() { return $this->q; }
  }

  $faker = Faker\Factory::create();
  $categories = array(new category("Droits humains"), new category("Société"), new category("Bien-être animal"), new category("Environement"), new category("Santé"), new category("Alimentation"), new category("Bien de conso."));

?>
<? include $_GET['page'] . '.php' ?>