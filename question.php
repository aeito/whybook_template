  <? snippet('header') ?>

  <? snippet('nav') ?>

  <section id="the-section">
    
    <div class="col six right">
      
      <article id="the-question" class="question">

        <h1><?= $question->content() ?></h1>
        <h2><?= $question->author() ?></h2>
        <h4><?= $question->date() ?></h4>
        <h3>To: <?= $question->recipient() ?></h3>

      </article><!-- /the-article -->

      <article id="the-answer" class="answer">
        
        <h4><?= $answer->date() ?></h4>

        <?= $answer->content() ?>

      </article><!-- /the-answer -->

    </div>

    <div class="col one left"> 

      <nav id="the-nav">

        <ul class="categories">
          <? foreach ($question->categories as $category): ?>
          <li>#<a href="/?filter=<?= $category->safename() ?>"><?= $category->name() ?></a></li>
          <? endforeach ?>
        </ul>
      
        <ul class="menu action">
          <li><button class="moutonner" type="button" >Moutonner</button></li>
          <li><button type="button" >Share</button></li>
        </ul>

      </nav><!-- /the-nav -->

      <aside id="the-context">
        <?= $question->context() ?>
      </aside>

    </div>

    <? snippet('footer.question') ?>

  </section><!-- /the-section -->

  <? snippet('footer') ?>