<? snippet('header') ?>

<? snippet('nav') ?>

<? snippet('message') ?>

<section id="the-content" class="clearfix">
  
  <div class="col-sm-1 col-md-1">
    <? snippet('filter') ?>
    <? snippet('monitoring') ?>
  </div>

  <div class="col-sm-6 col-md-6">

    <article class="question-card col-sm-3 col-md-2">
      <div class="in">
        <header>
          <h1 class="question">Comment justifiez-vous d’avoir voté NON à la loi européenne interdisant la pêche en eau profonde ?</h1>
        </header>
        <section>
          <h2 class="name">Grégory P.</h2>
          <h4 class="date">à l’instant</h4>
          <h3 class="to"><small>à</small>  Isabelle Thomas,<br> <small>Parlementaire européen</small></h3>
          <p class="moutonnez"><a href="#" class="btn btn-primary" role="button">M<span>oO</span>tonnez</a>
        </section>
      </div>
    </article>
    <article class="question-card col-sm-3 col-md-2">
      <div class="in">
        <header>
          <h1 class="question">D’où vient et comment est extrait l’or de vos bijoux ?</h1>
        </header>
        <section>
          <h2 class="name">S. Galy</h2>
          <h4 class="date">hier</h4>
          <h3 class="to"><small>à</small> Gérard Auriet,<br> <small>P.D.G., Mauboussin</small></h3>
          <p class="moutonnez"><a href="#" class="btn btn-primary" role="button">M<span>oO</span>tonnez</a>
        </section>
      </div>
    </article>
    <article class="question-card col-sm-3 col-md-2">
      <div class="in">
        <header>
          <h1 class="question">Que comptez-vous faire contre la flambée des prix des loyers ?</h1>
        </header>
        <section>
          <h2 class="name">Grégory P.</h2>
          <h4 class="date">il y a 2 jours</h4>
          <h3 class="to"><small>à</small> Alain Juppé,<br> <small>Maire, Bordeaux</small></h3>
          <p class="moutonnez"><a href="#" class="btn btn-primary" role="button">M<span>oO</span>tonnez</a>
        </section>
      </div>
    </article>
    <article class="question-card col-sm-3 col-md-2">
      <div class="in">
        <header>
          <h1 class="question">Garantissez-vous l’innocuité du maïs transgénique TC1507 ?</h1>
        </header>
        <section>
          <h2 class="name">Amandine L.</h2>
          <h4 class="date">il y a 3 jours</h4>
          <h3 class="to"><small>à</small> Hugh Grant,<br> <small>P.D.G., Monsanto</small></h3>
          <p class="moutonnez"><a href="#" class="btn btn-primary" role="button">M<span>oO</span>tonnez</a>
        </section>
      </div>
    </article>

    <?
    $d = range(4,31);
    shuffle( $d );
    $d = array_slice($d,0,16);
    sort( $d );
    $n=0; while( $n < 11 ) : $n++ ?>
    <article class="question-card col-sm-3 col-md-2">
      <div class="in">
        <header>
          <h1 class="question"><?= $faker->text(160) ?></h1>
        </header>
        <section>
          <h2 class="name"><?= $faker->name() ?></h2>
          <h4 class="date">il y a <?= $d[$n] ?> jours</h4>
          <h3 class="to"><small>à</small> <?= $faker->name() ?>,<br> <small><?= $faker->company() ?></small></h3>
          <p class="moutonnez"><a href="#" class="btn btn-primary" role="button">M<span>oO</span>tonnez</a>
        </section>
      </div>
    </article>
    <? endwhile ?>
  </div>

</section><!-- /the-section -->

<? snippet('grid') ?>

<? snippet('footer') ?>